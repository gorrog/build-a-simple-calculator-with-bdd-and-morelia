#!/bin/bash

docker build -t calculator-bdd-example .

docker run -it --rm calculator-bdd-example python calculate.py $1 $2 $3
