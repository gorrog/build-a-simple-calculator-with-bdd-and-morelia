import sys
from ast import literal_eval
from contextlib import suppress

def calculate(number1, number2, operator):
    pass

if __name__ == '__main__':
    if not len(sys.argv) == 4:
        error = "\nError! You must supply 3 arguments to this function.\n\n"
        sys.stderr.write(error)
    else:
        args = []
        for arg in sys.argv:
            with suppress(Exception):
                arg = literal_eval(arg)
            args.append(arg)
        sys.stdout.write(calculate(args[1],args[2],args[3]) + "\n")
