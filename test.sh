#!/bin/bash

docker build -t calculator-bdd-example .

docker run -it --rm calculator-bdd-example python -m unittest
