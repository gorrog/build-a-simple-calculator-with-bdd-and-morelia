from unittest import TestCase
from morelia import run

class ArithmeticTestCase(TestCase):

    def test_basic_arithmetic(self):
        """Run tests for the requirements in arithmetic.feature """
        run('arithmetic.feature', self, verbose=True)
